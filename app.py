import asyncio
import os
import sys

import aiopg.sa
import logging

from aiohttp import web
from alembic import config

from config import PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_BASE, APP_HOST, APP_PORT
from urls import routes


class Application:
    def __init__(self):
        self.__loop = asyncio.get_event_loop()

    @staticmethod
    async def _init_pg(app):
        engine = await aiopg.sa.create_engine(
            database=PG_BASE,
            user=PG_USER,
            password=PG_PASSWORD,
            host=PG_HOST,
            port=PG_PORT,
        )
        app['db_engine'] = engine

    @staticmethod
    async def _close_pg(app):
        app['db_engine'].close()
        await app['db_engine'].wait_closed()

    def run(self):
        app = web.Application()
        app.router.add_routes(routes)

        app.on_startup.append(self._init_pg)
        app.on_cleanup.append(self._close_pg)

        self.__loop.create_task(
            web.run_app(app, host=APP_HOST, port=APP_PORT))


if __name__ == '__main__':
    logging.basicConfig(
        stream=sys.stdout,
        level=os.getenv('LOG_LEVEL', logging.DEBUG),
        format='%(asctime)-15s %(name)-5s %(levelname)-8s %(message)s',
    )

    alembicArgs = ['--raiseerr', 'upgrade', 'head']
    config.main(argv=alembicArgs)

    Application().run()
