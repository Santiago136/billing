import os

APP_HOST = '0.0.0.0'
APP_PORT = 8080

PG_BASE = os.getenv('PG_BASE', 'billing')
PG_USER = os.getenv('PG_USER', 'postgres')
PG_PASSWORD = os.getenv('PG_PASSWORD')
PG_HOST = os.getenv('PG_HOST', 'localhost')
PG_PORT = os.getenv('PG_PORT', '5432')
