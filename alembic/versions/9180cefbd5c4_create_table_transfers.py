"""create table transfers

Revision ID: 9180cefbd5c4
Revises: 2f171c97028f
Create Date: 2019-07-15 22:16:53.516271

"""
import sqlalchemy as sa

from alembic import op
from sqlalchemy import func


# revision identifiers, used by Alembic.
revision = '9180cefbd5c4'
down_revision = '2f171c97028f'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'transfers',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('wallet_from', sa.Integer),
        sa.Column('wallet_to', sa.Integer, nullable=False),
        sa.Column('sum', sa.Numeric, nullable=False),
        sa.Column('currency', sa.String(3), nullable=False),
        sa.Column('type', sa.String(20)),
        sa.Column('created', sa.DateTime, server_default=func.now()),
    )

    op.create_foreign_key(u'wallet_from_fkey', 'transfers', 'wallets', ['wallet_from'], ['id'])
    op.create_foreign_key(u'wallet_to_fkey', 'transfers', 'wallets', ['wallet_to'], ['id'])


def downgrade():
    op.drop_constraint(u'wallet_from_fkey', 'transfers', type='foreignkey')
    op.drop_constraint(u'wallet_to_fkey', 'transfers', type='foreignkey')

    op.drop_table('transfers')
