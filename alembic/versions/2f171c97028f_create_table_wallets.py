"""create table wallets

Revision ID: 2f171c97028f
Revises: 
Create Date: 2019-07-15 21:27:00.427812

"""
import sqlalchemy as sa

from alembic import op
from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = '2f171c97028f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'wallets',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False, unique=True),
        sa.Column('city', sa.String(200), nullable=False),
        sa.Column('currency', sa.String(3), nullable=False),
        sa.Column('balance', sa.Numeric, default=0),
        sa.Column('created', sa.DateTime, server_default=func.now()),
        sa.Column('updated', sa.DateTime, onupdate=func.now(), server_default=func.now()),
    )


def downgrade():
    op.drop_table('wallets')
