"""create table rates

Revision ID: 0a8747ff7dee
Revises: 9180cefbd5c4
Create Date: 2019-07-15 22:43:25.801084

"""
import sqlalchemy as sa

from alembic import op
from sqlalchemy import func


# revision identifiers, used by Alembic.
revision = '0a8747ff7dee'
down_revision = '9180cefbd5c4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'rates',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('currency', sa.String(3), nullable=False),
        sa.Column('rate_usd', sa.Numeric, nullable=False),
        sa.Column('created', sa.DateTime, server_default=func.now()),
    )


def downgrade():
    op.drop_table('rates')
