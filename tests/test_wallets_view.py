from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from unittest.mock import patch

from utils.wallet_operator import WalletOperator
from views.wallets_view import WalletsView


class DuplicatePaymentViewTestCase(AioHTTPTestCase):

    url = '/billing/wallets'

    async def get_application(self):
        app = web.Application()
        app.router.add_view(self.url, WalletsView)
        app['db_engine'] = None
        return app

    @unittest_run_loop
    async def test_successful_case(self):
        async def test_create(obj, name, city, currency):
            return 1

        with patch.object(WalletOperator, 'create', test_create):
            data = {'name': 'test name', 'city': 'test city', 'currency': 'USD'}

            response = await self.client.post(self.url, json=data)
            assert response.status == 200

            response = await response.json()
            assert response.get('wallet_id') == 1

    @unittest_run_loop
    async def test_less_params(self):
        async def test_create(obj, name, city, currency):
            return 1

        with patch.object(WalletOperator, 'create', test_create):
            data = {'name': 'test name', 'city': 'test city'}

            response = await self.client.post(self.url, json=data)
            assert response.status == 400

            response = await response.json()
            assert response.get('msg') == "'currency' is a required property"

    @unittest_run_loop
    async def test_wrong_currency(self):
        async def test_create(obj, name, city, currency):
            return 1

        with patch.object(WalletOperator, 'create', test_create):
            data = {'name': 'test name', 'city': 'test city', 'currency': 'DNR'}

            response = await self.client.post(self.url, json=data)
            assert response.status == 400

            response = await response.json()
            assert response.get('msg') == "Currency is not supported."

    @unittest_run_loop
    async def test_wrong_type_params(self):
        async def test_create(obj, name, city, currency):
            return 1

        with patch.object(WalletOperator, 'create', test_create):
            data = {'name': 'test name', 'city': 'test city', 'currency': 322}

            response = await self.client.post(self.url, json=data)
            assert response.status == 400

            response = await response.json()
            assert "Validation of property 'currency' failed" in response.get('msg')
