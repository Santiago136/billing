import sqlalchemy as sa

from sqlalchemy import func


wallets = sa.Table(
    'wallets', sa.MetaData(),
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(50), nullable=False),
    sa.Column('city', sa.String(200), nullable=False),
    sa.Column('currency', sa.String(3), nullable=False),
    sa.Column('balance', sa.Numeric, default=0),
    sa.Column('created', sa.DateTime, server_default=func.now()),
    sa.Column('updated', sa.DateTime, server_default=func.now(), onupdate=func.now()))

transfers = sa.Table(
    'transfers', sa.MetaData(),
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('wallet_from', sa.Integer),
    sa.Column('wallet_to', sa.Integer, nullable=False),
    sa.Column('sum', sa.Numeric, nullable=False),
    sa.Column('currency', sa.String(3), nullable=False),
    sa.Column('type', sa.String(20)),
    sa.Column('created', sa.DateTime, server_default=func.now()))

rates = sa.Table(
    'rates', sa.MetaData(),
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('currency', sa.String(3), nullable=False),
    sa.Column('rate_usd', sa.Numeric, nullable=False),
    sa.Column('created', sa.DateTime, server_default=func.now()))
