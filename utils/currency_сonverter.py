from decimal import Decimal, ROUND_DOWN

from aiopg.sa.engine import Engine

from utils.const import CURRENCIES
from utils.exceptions import ClientError
from utils.tables import rates


class CurrencyConverter:
    def __init__(self, db_engine: Engine):
        self._db_engine = db_engine

    async def convert(self, currency_from: str, currency_to: str, balance: Decimal):
        if currency_from not in CURRENCIES or currency_to not in CURRENCIES:
            raise ClientError('Currency is not supported')

        if currency_from is 'USD':
            rate = await self._get_usd_rate(currency_to)
            balance = balance * rate

            return balance.quantize(Decimal('.0001'), rounding=ROUND_DOWN)

        elif currency_to is 'USD':
            rate = await self._get_usd_rate(currency_from)
            balance = balance / rate

            return balance.quantize(Decimal('.0001'), rounding=ROUND_DOWN)

        else:
            rate_from = await self._get_usd_rate(currency_from)
            rate_to = await self._get_usd_rate(currency_to)
            balance = balance / rate_from * rate_to

            return balance.quantize(Decimal('.0001'), rounding=ROUND_DOWN)

    async def _get_usd_rate(self, currency: str):
        if currency == 'USD':
            return 1

        async with self._db_engine.acquire() as conn:
            cursor = await conn.execute(
                rates.select().where(rates.c.currency == currency).order_by(rates.c.created.desc()))
            record = await cursor.fetchone()

        return record[2]
