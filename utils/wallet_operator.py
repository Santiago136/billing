from decimal import Decimal

from psycopg2 import errors
from aiopg.sa.engine import Engine

from utils.currency_сonverter import CurrencyConverter
from utils.exceptions import WalletError
from utils.tables import wallets


class WalletOperator:
    def __init__(self, db_engine: Engine):
        self._db_engine = db_engine

    async def create(self, name: str, city: str, currency: str):
        async with self._db_engine.acquire() as conn:
            try:
                cursor = await conn.execute(wallets.insert().values(
                    name=name,
                    city=city,
                    currency=currency,
                ))
                record = await cursor.fetchone()
            except errors.UniqueViolation as e:
                raise WalletError(f'User with such name exists')

        return record[0]

    async def find(self, wallet_id: int):
        async with self._db_engine.acquire() as conn:
            cursor = await conn.execute(
                wallets.select().where(wallets.c.id == wallet_id))
            record = await cursor.fetchone()

            return dict(record)

    async def change_balance(self, wallet_id: int, balance: Decimal, currency: str):
        wallet = await self.find(wallet_id)

        if not wallet:
            raise WalletError(f'Wallet {wallet_id} not exist.')

        if currency != wallet['currency']:
            converter = CurrencyConverter(self._db_engine)
            balance = await converter.convert(currency, wallet['currency'], balance)

        new_balance = balance + wallet['balance']

        if new_balance < 0:
            raise WalletError('Balance will be less then 0')

        async with self._db_engine.acquire() as conn:
            await conn.execute(
                wallets.update().where(wallets.c.id == wallet['id']).values(balance=new_balance))

    async def transfer_funds(self, wallet_from: int, wallet_to: int, balance: Decimal, currency: str):

        wallet_from = await self.find(wallet_id=wallet_from)
        wallet_to = await self.find(wallet_id=wallet_to)

        if not wallet_to or not wallet_from:
            raise WalletError(f'Wallet {wallet_to} or {wallet_from} not exist')

        await self.change_balance(
            balance=-1 * balance,
            currency=currency,
            wallet_id=wallet_from['id'])

        await self.change_balance(
            balance=balance,
            currency=currency,
            wallet_id=wallet_to['id'])
