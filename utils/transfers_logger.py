from decimal import Decimal

from aiopg.sa.engine import Engine

from utils.tables import transfers


class TransfersLogger:

    def __init__(self, db_engine: Engine):
        self._db_engine = db_engine

    async def log(
            self,
            wallet_to: int,
            transfer_sum: Decimal,
            currency: str,
            transfer_type: str,
            wallet_from: int = None):

        async with self._db_engine.acquire() as conn:
            await conn.execute(transfers.insert().values(
                wallet_from=wallet_from,
                wallet_to=wallet_to,
                sum=transfer_sum,
                currency=currency,
                type=transfer_type,
            ))
