import json

from aiohttp.web_exceptions import HTTPBadRequest


class ClientError(HTTPBadRequest):
    def __init__(self, msg: str):
        super().__init__()
        self.status_code = 400
        self.content_type = 'application/json'
        self.body = json.dumps({'msg': msg})


class WalletError(Exception):
    def __init__(self, msg: str):
        self.message = msg
