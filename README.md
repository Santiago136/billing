# Easy billing

## Application for money transferring and generating reports

###To run:
* Host PostgreSQL DB with database "billing"
* Set connection setup at config.py
* Run application to apply DB migrations `python app.py` and listen
* If it will not work, try setup file `alembic/env.py` 

###Endpoints

####POST `/billing/wallets` - create a new wallet
#####Params:
* name - user name, string
* city - user city, string
* currency - ticker, string
#####Returns:
* wallet_id - wallet unique number, integer

####PUT `/billing/wallets`
#####Params:
* wallet_id - wallet unique number, integer
* balance - decimal sum for deposit, string
* currency - ticker, string

###PUT `/billing/transfers` - deposit money
####Params:
* wallet_from - sender wallet id, integer
* wallet_to - resiever wallet id, integer
* balance - decimal sum for deposit, string
* currency - ticker, string


###POST `/billing/rates` - upload new rates to USD
####Params: 
* rate_usd - currency rate to USD, string
* currency - ticker, string

###GET `/billing/reports` - upload XML or CSV reports
####Params: 
* name - user name, string
* type - report type: `xml` or `csv`, string
* from - starting date `yyyy-mm-dd`, string
* to - last date date `yyyy-mm-dd`, string

