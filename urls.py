from aiohttp import web

from views.rates_view import RatesView
from views.reports_view import ReportsView
from views.transfers_view import TransfersView
from views.wallets_view import WalletsView

routes = [
    web.view('/billing/wallets', WalletsView),
    web.view('/billing/transfers', TransfersView),
    web.view('/billing/rates', RatesView),
    web.view('/billing/reports', ReportsView),
]
