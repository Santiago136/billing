from decimal import Decimal

from aiohttp import web

from utils.exceptions import WalletError, ClientError
from utils.transfers_logger import TransfersLogger
from utils.wallet_operator import WalletOperator
from views.base_view import BaseView


class TransfersView(BaseView):
    async def put(self):
        validation_schema = {
            'type': 'object',
            'properties': {
                'wallet_from': {'type': 'integer'},
                'wallet_to': {'type': 'integer'},
                'currency': {'type': 'string', 'maxLength': 3},
                'balance': {'type': 'string', 'maxLength': 10, 'pattern': r'^\d+(?:\.\d+)?$'},
            },
            'required': ['wallet_from', 'wallet_to', 'currency', 'balance'],
            'additionalProperties': False,
        }

        data = await self._validate_request_params(validation_schema)
        data['balance'] = Decimal(data['balance'])

        operator = WalletOperator(self._db_engine)

        try:
            await operator.transfer_funds(**data)
        except WalletError as ex:
            raise ClientError(ex.message)

        await TransfersLogger(self._db_engine).log(
            wallet_from=data['wallet_from'],
            wallet_to=data['wallet_to'],
            currency=data['currency'],
            transfer_sum=data['balance'],
            transfer_type='transfer',
        )

        return web.Response()
