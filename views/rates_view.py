from decimal import Decimal

from aiohttp import web

from utils.exceptions import ClientError
from utils.tables import rates
from views.base_view import BaseView


class RatesView(BaseView):
    async def post(self):
        validation_schema = {
            'type': 'object',
            'properties': {
                'rate_usd': {'type': 'string', 'maxLength': 5, 'pattern': r'^\d+(?:\.\d+)?$'},
                'currency': {'type': 'string', 'maxLength': 3},
            },
            'required': ['rate_usd', 'currency'],
            'additionalProperties': False
        }

        data = await self._validate_request_params(validation_schema)

        if data['currency'] is 'USD':
            raise ClientError('Currency must not be USD.')

        data['rate_usd'] = Decimal(data['rate_usd'])

        async with self._db_engine.acquire() as conn:
            cursor = await conn.execute(rates.insert().values(**data))
            record = await cursor.fetchone()
            rate_id = record[0]

        self._logger.debug(f'Rate added: {rate_id} {data["currency"]} {data["rate_usd"]}')

        return web.Response()
