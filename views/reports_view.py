import csv
import io

from datetime import datetime

from aiohttp import web
from dict2xml import dict2xml
from sqlalchemy import or_, and_

from utils.exceptions import ClientError
from utils.tables import wallets, transfers
from views.base_view import BaseView


class ReportsView(BaseView):
    async def get(self):
        date_schema = {
            'type': 'string',
            'minLength': 10,
            'maxLength': 10,
            'pattern': r'([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))',
            'message': 'must be date with yyyy-mm-dd format'
        }

        validation_schema = {
            'type': 'object',
            'properties': {
                'name': {'type': 'string', 'maxLength': 50},
                'type': {'type': 'string', 'maxLength': 3},
                'from': date_schema,
                'to': date_schema,
            },
            'required': ['name', 'type'],
            'additionalProperties': False,
        }

        data = await self._validate_request_params(validation_schema, is_query=True)

        if data['type'] not in ['csv', 'xml']:
            raise ClientError('Report type is not supported.')

        date_from = datetime.strptime(data['from'], '%Y-%m-%d') if data.get('from') else None
        date_to = datetime.strptime(data['to'], '%Y-%m-%d') if data.get('to') else None

        records = await self._get_records(data['name'], date_from, date_to)

        if not records:
            return web.Response(status=404)

        if data['type'] == 'xml':
            xml = '<?xml version="1.0" encoding="UTF-8"?>\n<transfers>'
            for record in records:
                xml += f'\n<transfer>\n{dict2xml(record)}\n</transfer>'

            xml += '\n</transfers>'

            return web.Response(text=xml, content_type='application/xml')
        else:
            with io.StringIO() as file:
                writer = csv.DictWriter(file, records[0].keys(), quoting=csv.QUOTE_ALL)
                writer.writeheader()

                for record in records:
                    writer.writerow(record)

                contents = file.getvalue()

            return web.Response(text=contents, content_type='text/csv')

    async def _get_records(self, user_name, date_from: datetime = None, date_to: datetime = None):
        async with self._db_engine.acquire() as conn:
            cursor = await conn.execute(
                wallets.select().where(wallets.c.name == user_name))
            record = await cursor.fetchone()
            wallet = dict(record)

            condition = or_(transfers.c.wallet_from == wallet['id'], transfers.c.wallet_to == wallet['id'])

            if date_from:
                condition = and_(transfers.c.created >= date_from, condition)

            if date_to:
                condition = and_(transfers.c.created < date_to, condition)

            cursor = await conn.execute(transfers.select().where(condition).order_by(transfers.c.id))

            return await cursor.fetchall()
