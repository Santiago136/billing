from decimal import Decimal

from aiohttp import web

from utils.exceptions import WalletError, ClientError
from utils.transfers_logger import TransfersLogger
from utils.wallet_operator import WalletOperator
from views.base_view import BaseView


class WalletsView(BaseView):
    async def post(self):
        validation_schema = {
            'type': 'object',
            'properties': {
                'name': {'type': 'string', 'maxLength': 50},
                'city': {'type': 'string', 'maxLength': 200},
                'currency': {'type': 'string', 'maxLength': 3},
            },
            'required': ['name', 'city', 'currency'],
            'additionalProperties': False
        }

        data = await self._validate_request_params(validation_schema)

        try:
            wallet_id = await WalletOperator(self._db_engine).create(**data)
        except WalletError as e:
            raise ClientError(e.message)

        self._logger.info(f'Wallet created: {wallet_id}')

        return web.json_response({'wallet_id': wallet_id})

    async def put(self):
        validation_schema = {
            'type': 'object',
            'properties': {
                'wallet_id': {'type': 'integer'},
                'balance': {'type': 'string', 'maxLength': 10, 'pattern': r'^\d+(?:\.\d+)?$'},
                'currency': {'type': 'string', 'maxLength': 3},
            },
            'required': ['wallet_id', 'balance', 'currency'],
            'additionalProperties': False,
        }

        data = await self._validate_request_params(validation_schema)
        data['balance'] = Decimal(data['balance'])

        try:
            await WalletOperator(self._db_engine).change_balance(**data)
        except WalletError as ex:
            raise ClientError(ex.message)

        await TransfersLogger(self._db_engine).log(
            wallet_to=data['wallet_id'],
            currency=data['currency'],
            transfer_sum=data['balance'],
            transfer_type='deposit',
        )

        self._logger.info(f'Wallet {data["wallet_id"]} balance: {data["balance"]}')

        return web.Response()
