import json
import logging

from aiohttp.web import View
from json_payload_validator import validate as json_schema_validate

from utils.const import CURRENCIES
from utils.exceptions import ClientError


class BaseView(View):
    def __init__(self, request):
        super().__init__(request)
        self._db_engine = self.request.app['db_engine']
        self._logger = logging.getLogger(self.__class__.__name__)

    async def _validate_request_params(self, schema, is_query=False):

        if is_query:
            params = dict(self.request.query)
        else:
            try:
                params = await self.request.json()
            except (json.decoder.JSONDecodeError, TypeError):
                raise ClientError('Request is malformed; could not decode JSON object.')

        error = json_schema_validate(params, schema)
        if error:
            raise ClientError(str(error))

        if params.get('currency') is not None and params['currency'] not in CURRENCIES:
            raise ClientError('Currency is not supported.')

        return params
